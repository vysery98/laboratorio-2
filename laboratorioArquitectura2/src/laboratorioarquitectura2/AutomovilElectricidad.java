/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package laboratorioarquitectura2;

/**
 *
 * @author castle
 */
public class AutomovilElectricidad extends Automovil{
    public AutomovilElectricidad (String modelo, String color, double potencia, int capacidad){
        super(modelo, color, potencia, capacidad);
    }
    
    public void mostrarEspecificaciones(){
        System.out.printf("---Automóvil Eléctrico---\nModelo -> %s\nColor -> %s"
                + "\nPotencia -> %.2f\nCapacidad -> %d\n\n", modelo, color,
                potencia, capacidad);
    }
}
