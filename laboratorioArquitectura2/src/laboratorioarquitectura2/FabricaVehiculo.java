/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package laboratorioarquitectura2;

/**
 *
 * @author castle
 */
public interface FabricaVehiculo {
    // Ensamblaje de Automóvil
    Automovil creaAutomovil(String modelo, String color, double potencia, int capacidad);
    // Ensamblaje de Scooter
    Scooter creaScooter(String modelo, String color, double potencia);
}
