/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package laboratorioarquitectura2;

/**
 *
 * @author castle
 */
public class FabricaVehiculoGasolina implements FabricaVehiculo{
    // Ensamblaje de Automóvil
    public Automovil creaAutomovil(String modelo, String color, double potencia, int capacidad){
        return new AutomovilElectricidad(modelo, color, potencia, capacidad);
    }
    
    // Ensamblaje de Scooter
    public Scooter creaScooter(String modelo, String color, double potencia){
        return new ScooterGasolina(modelo, color, potencia);
    }
}
