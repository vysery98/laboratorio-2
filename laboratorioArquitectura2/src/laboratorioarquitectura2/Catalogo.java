/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package laboratorioarquitectura2;

import java.util.Scanner;

/**
 *
 * @author castle
 */
public class Catalogo {

    public static int numAutos = 2;
    public static int numScooters = 2;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declaración de variables
        boolean bandera = true;
        int opcion;
        Automovil[] carros = new Automovil [numAutos];
        Scooter[] motonetas = new Scooter [numAutos];
        FabricaVehiculo fabrica = null;
        
        // Scanner para lectura de datos
        Scanner scan = new Scanner(System.in);
        
        String menu = "Ingrese la opción según el tipo de vehículo:\n(1) Vehí"
                + "culo eléctrico\n(2) Vehículo a gasolina\nOpción = ";
        System.out.println("\n----- C A T Á L O G O -----");
        
        // Elección de tipo de vehículo
        do{
            System.out.print(menu);
            opcion = scan.nextInt();
            switch(opcion){
                case 1:
                    fabrica = new FabricaVehiculoElectricidad();
                    bandera = false;
                    break;
                
                case 2:
                    fabrica = new FabricaVehiculoGasolina();
                    bandera = false;
                    break;

                default:
                    System.out.println("Opción no válida, ingrese nuevamente.");
                    break;
            }
        }while(bandera != false);
        
    // Ingreso de datos
        // Autos
        for (int i = 0; i < carros.length; i++) {
            if ((i % 2) == 0){
                carros[i] = fabrica.creaAutomovil("Mustang", "blanco", 210, 5);
            } else {
                carros[i] = fabrica.creaAutomovil("Byd han", "rojo", 163, 5);
            }
        }
        // Scooters
        for (int i = 0; i < motonetas.length; i++) {
            if ((i % 2) == 0){
                motonetas[i] = fabrica.creaScooter("Honda", "azul", 14.75);
            } else {
                motonetas[i] = fabrica.creaScooter("Yamaha", "negro", 57.8);
            }
        }
        
    // SALIDA
        // Autos
        for (Automovil auto: carros){
            auto.mostrarEspecificaciones();
        }
        // Scooters
        for (Scooter scooter: motonetas){
            scooter.mostrarEspecificaciones();
        }
    }
    
}
