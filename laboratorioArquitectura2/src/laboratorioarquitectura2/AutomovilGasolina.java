/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package laboratorioarquitectura2;

/**
 *
 * @author castle
 */
public class AutomovilGasolina extends Automovil{
    public AutomovilGasolina (String modelo, String color, double potencia, int capacidad){
        super(modelo, color, potencia, capacidad);
    }
    
    public void mostrarEspecificaciones(){
        System.out.printf("---Automóvil a Gasolina---\nModelo -> %s\nColor -> "
                + "%s\nPotencia -> %.2f\nCapacidad -> %d\n\n", modelo, color,
                potencia, capacidad);
    }
}
