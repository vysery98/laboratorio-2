/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package laboratorioarquitectura2;

/**
 *
 * @author castle
 */
public class ScooterElectricidad extends Scooter{
    public ScooterElectricidad (String modelo, String color, double potencia){
        super(modelo, color, potencia);
    }
    
    public void mostrarEspecificaciones(){
        System.out.printf("---Automóvil Eléctrico---\nModelo -> %s\nColor -> %s"
                + "\nPotencia -> %.2f\n\n", modelo, color, potencia);
    }
}
