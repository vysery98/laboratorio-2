/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package laboratorioarquitectura2;

/**
 *
 * @author castle
 */
public abstract class Automovil {
    protected String modelo;
    protected String color;
    protected double potencia;
    protected int capacidad;
    
    public Automovil(String modelo, String color, double potencia, int capacidad) {
        this.modelo = modelo;
        this.color = color;
        this.potencia = potencia;
        this.capacidad = capacidad;
    }
    
    public abstract void mostrarEspecificaciones();
}
