/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package laboratorioarquitectura2;

/**
 *
 * @author castle
 */
public abstract class Scooter {
    protected String modelo;
    protected String color;
    protected double potencia;
    
    public Scooter(String modelo, String color, double potencia) {
        this.modelo = modelo;
        this.color = color;
        this.potencia = potencia;
    }
    
    public abstract void mostrarEspecificaciones();
}
